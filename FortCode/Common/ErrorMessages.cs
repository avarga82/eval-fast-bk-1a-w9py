﻿namespace FortCode.Common
{
    public static class ErrorMessages
    {
        public const string InternalServerError = "Internal Server Error occurred.";
        public const string UnexpectedException = "An unexpected FortCode exception occurred!";
    }
}