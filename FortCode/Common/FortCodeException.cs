﻿using System;
using System.Net;

namespace FortCode.Common
{
    public class FortCodeException: ApplicationException
    {
        /// <summary>
        /// The http status code to set in the response
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }

        /// <summary>
        /// A custom error code to expose to the clients
        /// </summary>
        public string ErrorCode { get; set; }

        /// <summary>
        /// Creates a EditorServiceException object
        /// </summary>
        /// <param name="httpStatusCode">The http status code to set in the response</param>
        /// <param name="errorCode">An optional custom error code to expose to the clients</param>
        /// <param name="message">A message to expose to the clients, describing the error</param>
        public FortCodeException(HttpStatusCode httpStatusCode, string message, string errorCode = null) : base(message)
        {
            HttpStatusCode = httpStatusCode;
            ErrorCode = errorCode;
        }
    }
}