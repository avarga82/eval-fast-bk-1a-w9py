﻿using System;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace FortCode.Common
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionMiddleware> _logger;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ErrorMessages.UnexpectedException);
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var message = ErrorMessages.InternalServerError; // Our generic catch-all error.
            var errorCode = string.Empty;

            context.Response.ContentType = MediaTypeNames.Application.Json;
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            if (exception is FortCodeException customException)
            {
                context.Response.StatusCode = (int)customException.HttpStatusCode;
                message = customException.Message;
                errorCode = customException.ErrorCode;
            }

            await context.Response.WriteAsync(JsonConvert.SerializeObject(new ErrorDetails
            {
                Message = message,
                ErrorCode = errorCode,
            }));
        }
    }
}