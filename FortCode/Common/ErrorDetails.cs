﻿using Newtonsoft.Json;

namespace FortCode.Common
{
    public class ErrorDetails
    {
        [JsonProperty("message")]
        public string Message { get; set; }
        
        [JsonProperty("errorCode")]
        public string ErrorCode { get; set; }

    }
}
