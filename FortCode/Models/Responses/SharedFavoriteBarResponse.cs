﻿using System;

namespace FortCode.Models.Responses
{
    public class SharedFavoriteBarResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int CountOfSharedFavorites { get; set; }
    }
}