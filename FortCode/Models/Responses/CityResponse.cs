﻿using System;

namespace FortCode.Models.Responses
{
    public class CityResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
    }
}