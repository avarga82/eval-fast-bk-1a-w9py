﻿using System;

namespace FortCode.Models.Responses
{
    public class BarResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public CityResponse City { get; set; }
    }
}