﻿namespace FortCode.Models.Responses
{
    public class AuthResponse
    {
        public UserResponse User { get; set; }
        public string Token { get; set; }
    }
}