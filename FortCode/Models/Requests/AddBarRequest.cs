﻿using System;

namespace FortCode.Models.Requests
{
    public class AddBarRequest
    {
        public Guid BarId { get; set; }
        public string FavoriteDrink { get; set; }
    }
}