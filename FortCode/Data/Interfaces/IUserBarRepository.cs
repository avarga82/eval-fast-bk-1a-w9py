using FortCode.Data.Models;

namespace FortCode.Data.Interfaces
{
    public interface IUserBarRepository : IGenericRepository<UserBar>
    {
    }
}