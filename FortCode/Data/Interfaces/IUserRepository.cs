using FortCode.Data.Models;

namespace FortCode.Data.Interfaces
{
    public interface IUserRepository : IGenericRepository<User>
    {
    }
}