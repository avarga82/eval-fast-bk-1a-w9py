﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FortCode.Data.Interfaces
{
    public interface IGenericRepository<TEntity>
        where TEntity : class
    {
        IQueryable<TEntity> GetAll();
 
        Task<TEntity> GetByIdAsync(Guid id);

        Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> predicate);
 
        Task AddAsync(TEntity entity);
 
        Task UpdateAsync(Guid id, TEntity entity);
 
        Task DeleteAsync(Guid id);
    }
}