﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FortCode.Data.Models;

namespace FortCode.Data.Interfaces
{
    public interface IBarRepository : IGenericRepository<Bar>
    {
        Task<IEnumerable<Bar>> GetFavoriteBarsAsync(Guid userId, Guid cityId);
    }
}