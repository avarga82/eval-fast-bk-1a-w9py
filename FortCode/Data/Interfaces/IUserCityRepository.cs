using FortCode.Data.Models;

namespace FortCode.Data.Interfaces
{
    public interface IUserCityRepository : IGenericRepository<UserCity>
    {
    }
}