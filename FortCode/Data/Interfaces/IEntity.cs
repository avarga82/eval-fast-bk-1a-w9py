using System;

namespace FortCode.Data.Interfaces
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}