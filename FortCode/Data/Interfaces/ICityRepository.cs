﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FortCode.Data.Models;

namespace FortCode.Data.Interfaces
{
    public interface ICityRepository : IGenericRepository<City>
    {
        Task<IEnumerable<City>> GetFavoriteCities(Guid userId);
    }
}