﻿using System;
using System.ComponentModel.DataAnnotations;
using FortCode.Data.Interfaces;

namespace FortCode.Data.Models
{
    public abstract class Entity: IEntity
    {
        [Key]
        [Required]
        public Guid Id { get; set; }
    }
}