﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FortCode.Data.Models
{
    [Table("UserBar")]
    public class UserBar: Entity
    {
        [Required]
        public Guid UserId { get; set; }

        [Required]
        public Guid BarId { get; set; }
        
        [Required]
        public string FavoriteDrink { get; set; }
        
        public User User { get; set; }
        public Bar Bar { get; set; }
    }
}