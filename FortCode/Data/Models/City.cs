﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FortCode.Data.Models
{
    [Table("City")]
    public class City: Entity
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        
        [Required]
        public Guid CountryId { get; set; }
        
        public Country Country { get; set; }
        
        public ICollection<Bar> Bars { get; set; }
        public ICollection<UserCity> Users { get; set; }
    }
}