﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FortCode.Data.Models
{
    [Table("UserCity")]
    public class UserCity: Entity
    {
        [Required]
        public Guid UserId { get; set; }
        
        [Required]
        public Guid CityId { get; set; }
        
        public User User { get; set; }
        public City City { get; set; }
    }
}