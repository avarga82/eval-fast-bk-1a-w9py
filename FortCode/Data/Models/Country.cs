﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FortCode.Data.Models
{
    [Table("Country")]
    public class Country: Entity
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        
        public ICollection<City> Cities { get; set; }
    }
}