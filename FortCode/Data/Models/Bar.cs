﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FortCode.Data.Models
{
    [Table("Bar")]
    public class Bar: Entity
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        
        [Required]
        public Guid CityId { get; set; }
        
        public City City { get; set; }
        
        public ICollection<UserBar> Users { get; set; }
    }
}