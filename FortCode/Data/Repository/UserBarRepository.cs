﻿using FortCode.Data.Interfaces;
using FortCode.Data.Models;

namespace FortCode.Data.Repository
{
    public class UserBarRepository:  GenericRepository<UserBar>, IUserBarRepository
    {
        public UserBarRepository(FortCodeDbContext context) : base(context)
        {
        }
    }
}