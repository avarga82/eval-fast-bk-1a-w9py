﻿using FortCode.Data.Interfaces;
using FortCode.Data.Models;

namespace FortCode.Data.Repository
{
    public class UserRepository:  GenericRepository<User>, IUserRepository
    {
        public UserRepository(FortCodeDbContext context) : base(context)
        {
        }
    }
}