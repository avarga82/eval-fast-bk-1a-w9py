﻿using FortCode.Data.Interfaces;
using FortCode.Data.Models;

namespace FortCode.Data.Repository
{
    public class UserCityRepository:  GenericRepository<UserCity>, IUserCityRepository
    {
        public UserCityRepository(FortCodeDbContext context) : base(context)
        {
        }
    }
}