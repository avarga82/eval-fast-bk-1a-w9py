﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Data.Interfaces;
using FortCode.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Data.Repository
{
    public class BarRepository:  GenericRepository<Bar>, IBarRepository
    {
        public BarRepository(FortCodeDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Bar>> GetFavoriteBarsAsync(Guid userId, Guid cityId)
        {
            return await _context.UserBars
                .Include($"{nameof(Bar)}.{nameof(City)}")
                .Include($"{nameof(Bar)}.{nameof(City)}.{nameof(Country)}")
                .Where(c => c.UserId == userId && c.Bar.CityId == cityId)
                .Select(c => c.Bar)
                .ToListAsync();
        }
    }
}