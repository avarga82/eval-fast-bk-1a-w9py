﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Data.Interfaces;
using FortCode.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Data.Repository
{
    public class CityRepository:  GenericRepository<City>, ICityRepository
    {
        public CityRepository(FortCodeDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<City>> GetFavoriteCities(Guid userId)
        {
            return await _context.UserCities
                .Include($"{nameof(City)}.{nameof(Country)}")
                .Where(c => c.UserId == userId)
                .Select(c => c.City)
                .ToListAsync();
        }
    }
}