﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FortCode.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Data
{
    public class FortCodeDbContext : IdentityDbContext<User, IdentityRole<Guid>, Guid>
    {
        public FortCodeDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Bar> Bars { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        
        public DbSet<User> Users { get; set; }
        public DbSet<UserCity> UserCities { get; set; }
        public DbSet<UserBar> UserBars { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Country>()
                .HasData(SeedData.Countries);
            
            modelBuilder.Entity<City>()
                .HasData(SeedData.Cities);
            
            modelBuilder.Entity<Bar>()
                .HasData(SeedData.Bars);
        }
    }

    public static class SeedData
    {
        public static List<Country> Countries => new List<Country>()
        {
            new Country { Id = Guid.Parse("BBA848A6-591A-4F5E-8370-E581AF4B2419"), Name = "United States" },
            new Country { Id = Guid.Parse("F28D4CFD-F07B-4D82-A2B3-92E305FD7DFC"), Name = "Canada" },
            new Country { Id = Guid.Parse("22141D61-BB9B-4D09-BA03-6A34F1683ED6"), Name = "Mexico" },
            new Country { Id = Guid.Parse("C00C4CBA-DCA6-4B68-996D-33366C995F48"), Name = "Italy" },
            new Country { Id = Guid.Parse("9DA77535-6A92-43D1-A9FE-AAFD1988498B"), Name = "France" }
        };
        
        public static List<City> Cities => new List<City>()
        {
            new City { Id = Guid.Parse("76C6FACC-C96C-4625-8B50-83A7AFA493E4"), CountryId = Countries[0].Id, Name = "Philadelphia" },
            new City { Id = Guid.Parse("326CD7D8-634B-4422-A526-D87BE9F96F34"), CountryId = Countries[0].Id, Name = "Pittsburgh" },
            new City { Id = Guid.Parse("D0CDC9F3-8238-4D8C-AEA1-5CF72D415D0C"), CountryId = Countries[1].Id, Name = "Toronto" },
            new City { Id = Guid.Parse("B4D05BBF-4516-4AAF-9134-9893529AFDEB"), CountryId = Countries[1].Id, Name = "Vancouver" },
            new City { Id = Guid.Parse("EAB471B7-B0ED-419F-A63F-C2CE4EBC0236"), CountryId = Countries[2].Id, Name = "Oaxaca" },
            new City { Id = Guid.Parse("27E9814E-AE13-4038-B556-19CFD36FE597"), CountryId = Countries[2].Id, Name = "Cancun" },
            new City { Id = Guid.Parse("95DCE6DD-CCE1-4B6D-A282-F4C1CF01C0E0"), CountryId = Countries[3].Id, Name = "Rome" },
            new City { Id = Guid.Parse("DEE4E684-9874-4566-9453-BBA7595AE9A8"), CountryId = Countries[3].Id, Name = "Venice" },
            new City { Id = Guid.Parse("F129E1DC-29FC-459D-AF12-50EC2D14D98E"), CountryId = Countries[4].Id, Name = "Paris" },
            new City { Id = Guid.Parse("915999F9-D294-40F1-80B8-472012CBBA35"), CountryId = Countries[4].Id, Name = "Bourdeaux" }
        };
        
        public static List<Bar> Bars => new List<Bar>()
        {
            new Bar { Id = Guid.Parse("4267363F-C7CC-4626-B06F-F22044F6777A"), CityId = Cities[0].Id, Name = "Drama Club" },
            new Bar { Id = Guid.Parse("9FEBC78B-3542-4FF7-B287-D2212BA29095"), CityId = Cities[0].Id, Name = "Bottoms Up" },
            new Bar { Id = Guid.Parse("E3A5CE81-7788-4983-8BEA-73967CD57C03"), CityId = Cities[1].Id, Name = "Sip Sip Hooray" },
            new Bar { Id = Guid.Parse("28295A47-3C19-499A-AC17-20B1AC5D0E23"), CityId = Cities[1].Id, Name = "Beer Garden" }
        };
    }
}