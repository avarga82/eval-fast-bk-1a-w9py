# FortCode API

The following endpoint are available from the FortCode API. I've also included a Postman collection that can be imported from ```FortCode.postman_collection.json```.

## Users

### Register User

```
POST: api/user/register
```

#### Body
```
{
  "UserName": "string",
  "Email": "string",
  "Password": "string"
}
```

### Authenticate

```
POST: api/user/authenticate
```

#### Body
```
{
  "Email": "string",
  "Password": "string"
}
```

## Cities

### Get All Cities

```
GET: api/city
```
```Authentication: Bearer Token```


### Get Favorite Cities

```
GET: api/city/favorites
```
```Authentication: Bearer Token```

### Add Favorite City

```
POST: api/city/favorites/{cityId}
```
```Authentication: Bearer Token```

### Delete Favorite City

```
DELETE: api/city/favorites/{cityId}
```
```Authentication: Bearer Token```

## Bars

### Get All Bars

```
GET: api/bar
```
```Authentication: Bearer Token```


### Get Favorite Bars

```
GET: api/bar/favorites/{cityId}
```
```Authentication: Bearer Token```

### Get Shared Favorite Bars

```
GET: api/bar/favorites/{cityId}/shared
```
```Authentication: Bearer Token```

### Add Favorite Bar

```
POST: api/bar/favorites
```
```Authentication: Bearer Token```

#### Body
```
{
  "BarId": "guid",
  "FavoriteDrink": "string"
}
```

### Delete Favorite Bar

```
DELETE: api/bar/favorites/{barId}
```
```Authentication: Bearer Token```