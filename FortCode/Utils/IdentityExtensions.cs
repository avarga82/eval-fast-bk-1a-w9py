﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace FortCode.Utils
{
    public static class IdentityExtensions
    {
        public static Guid GetUserId(this HttpContext context)
        {
            if (context?.User == null)
            {
                return Guid.Empty;
            }

            var value = context.User.Claims.SingleOrDefault(c => c.Type == "Id")?.Value;
            
            return !string.IsNullOrEmpty(value) ? Guid.Parse(value) : Guid.Empty;
        }
    }
}