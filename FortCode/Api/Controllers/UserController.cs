﻿using System.Threading.Tasks;
using FortCode.Models.Requests;
using FortCode.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FortCode.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IIdentityService _identityService;

        public UserController(ILogger<UserController> logger, IIdentityService identityService)
        {
            _logger = logger;
            _identityService = identityService;
        }
        
        /// <summary>
        /// Requirement: A user should be able to authenticate with an email and password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate(AuthRequest request)
        {
            return Ok(await _identityService.AuthenticateAsync(request));
        }
           
        /// <summary>
        /// Requirement: A user should be able to create a user account
        /// </summary>
        /// <param name="createUser"></param>
        /// <returns></returns>
        [AllowAnonymous]  
        [HttpPost("register")]  
        public async Task<ActionResult> Register([FromBody] CreateUserRequest createUser)
        {
            return Ok(await _identityService.RegisterAsync(createUser));
        }
    }
}