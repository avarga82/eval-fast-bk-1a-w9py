﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FortCode.Models.Requests;
using FortCode.Models.Responses;
using FortCode.Services.Interfaces;
using FortCode.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FortCode.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class BarController : ControllerBase
    {
        private readonly ILogger<BarController> _logger;
        private readonly IBarService _barService;

        public BarController(ILogger<BarController> logger, IBarService barService)
        {
            _logger = logger;
            _barService = barService;
        }
        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BarResponse>>> Get()
        {
            return Ok(await _barService.GetAllBarsAsync());
        }

        /// <summary>
        /// Requirement: An authenticated user should be able to retrieve a list of their favorite bars in one of their favorite cities
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        [HttpGet("favorites/{cityId}")]
        public async Task<ActionResult<IEnumerable<BarResponse>>> GetFavorites(Guid cityId)
        {
            var userId = HttpContext.GetUserId();
            return Ok(await _barService.GetFavoriteBarsAsync(userId, cityId));
        }

        /// <summary>
        /// An authenticated user should be able to add a secret bar (speakeasys) they patron in their favorite cities
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        [HttpPost("favorites")]
        public async Task<ActionResult> AddFavorite(AddBarRequest request)
        {
            var userId = HttpContext.GetUserId();
            await _barService.AddFavoriteBarAsync(userId, request);
            return NoContent();
        }

        [HttpDelete("favorites/{barId}")]
        public async Task<ActionResult> DeleteFavorite(Guid barId)
        {
            var userId = HttpContext.GetUserId();
            await _barService.DeleteFavoriteBarAsync(userId, barId);
            return NoContent();
        }
        
        /// <summary>
        /// Requirement: An authenticated user should be able to query if any of their favorite bars are shared with
        /// other users in the system by returning the shared bar names and the number of other users that share it
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        [HttpGet("favorites/{cityId}/shared")]
        public async Task<ActionResult<IEnumerable<SharedFavoriteBarResponse>>> GetSharedFavorites(Guid cityId)
        {
            var userId = HttpContext.GetUserId();
            return Ok(await _barService.GetSharedFavoriteBarsAsync(userId, cityId));
        }
    }
}