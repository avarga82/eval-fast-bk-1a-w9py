﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FortCode.Models.Responses;
using FortCode.Services.Interfaces;
using FortCode.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FortCode.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class CityController : ControllerBase
    {
        private readonly ILogger<CityController> _logger;
        private readonly ICityService _cityService;

        public CityController(ILogger<CityController> logger, ICityService cityService)
        {
            _logger = logger;
            _cityService = cityService;
        }
        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CityResponse>>> Get()
        {
            return Ok(await _cityService.GetAllCitiesAsync());
        }

        /// <summary>
        /// Requirement: An authenticated user should be able to retrieve a list of their favorite cities
        /// </summary>
        /// <returns></returns>
        [HttpGet("favorites")]
        public async Task<ActionResult<IEnumerable<CityResponse>>> GetFavorites()
        {
            var userId = HttpContext.GetUserId();
            return Ok(await _cityService.GetFavoriteCitiesAsync(userId));
        }

        /// <summary>
        /// Requirement: An authenticated user should be able to add their favorite cities
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        [HttpPost("favorites/{cityId}")]
        public async Task<ActionResult> AddFavorite(Guid cityId)
        {
            var userId = HttpContext.GetUserId();
            await _cityService.AddFavoriteCityAsync(userId, cityId);
            return NoContent();
        }

        /// <summary>
        /// An authenticated user should be able to remove their favorite cities
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        [HttpDelete("favorites/{cityId}")]
        public async Task<ActionResult> DeleteFavorite(Guid cityId)
        {
            var userId = HttpContext.GetUserId();
            await _cityService.DeleteFavoriteCityAsync(userId, cityId);
            return NoContent();
        }
    }
}