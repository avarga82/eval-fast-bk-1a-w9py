# FORT Robotics Technical Assessment

## Summary
For this exercise I created 3 main controllers: Bar, City, and User. Each is responsible for handling the requests for the given entity type and returning appropriate data and status codes.

- The User controller currently exposes two endpoints: a Register method to create users and an Authorize method to log in a user and return an access token. Both of these allow anonymous access. Additional endpoints could've been created here to return users based on an ID, but that was out of scope for this exercise since all interaction with the API will be for the current user and that can be determined from the auth token.
- The Bar and City controllers both expose multiple endpoints to return data and add/delete data. The controllers contain an ```[Authorize]``` attribute which ensures they cannot be accessed anonymously. The default authentication scheme is set to JWT bearer authentication.
- The controllers are purposely lightweight and do not contain any logic. All business logic currently resides in the service layer, though this could be refactored into separate validation classes to fit future requirements. The service layer is also responsible for interacting with the data via the repositories and mapping EF models to requests/DTO's exposed by the API.
- The repositories inherit a generic base repository with methods commonly used for data access.
- The project is using Entity Framework Code-First and database migrations to manage the schema and data of the database. 


## Considerations and Exclusions

- There is room for additional validation throughout. I'm currently validating key data isn't missing but additional validation could be added for the following scenarios:
  - Field-level requirements on the database models (min, max, etc.).
  - Validation to ensure favorite bars can't be added if they're not in the user's list of favorite cities. The API currently accepts any bar provided the ID is valid.
- I prefer all errors/string values be kept in a static class. The ```ErrorMessages.cs``` contains a couple but all error messages should go here. This would make it much easier to enable multilingual support in the future or to simply see all possible user-facing errors in a single place.
- A lot more unit/integration tests could be added. In the interest of time I kept it simple and included tests for a single API controller. These tests could be duplicated for the other controllers and modified as necessary. These tests currently do not include (but should):
  - Unit testing the EF repositories. Additional tests should be created to ensure the data is being returned as expected.
  - Unit testing the service layer. Additional tests should be created to ensure the data is being returned as expected.
  - Modifying/mocking the HttpContext to include a valid user, so the API's ```GetUserId()``` method would return a valid value.
  - Unit/integration testing the middleware to ensure exceptions are mapped appropriately to HTTP responses and logging occurs successfully.
  - Unit testing to ensure the JWT token generation is correct.
- The project's target framework is .NET Core 3.1 which I left as-is. This could have been easily upgraded to the current version but I didn't have enough context on whether it made sense to do so. Perhaps the CI/CD pipelines or consumers of this are dependent on 3.1
