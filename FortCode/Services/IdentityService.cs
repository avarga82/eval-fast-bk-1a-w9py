﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FortCode.Common;
using FortCode.Data.Models;
using FortCode.Models.Requests;
using FortCode.Models.Responses;
using FortCode.Services.Interfaces;
using FortCode.Utils;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace FortCode.Services
{
    public class IdentityService : IIdentityService
    {
        private readonly ILogger<IdentityService> _logger;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly JwtTokenHelper _tokenHelper;

        public IdentityService(
            ILogger<IdentityService> logger, 
            UserManager<User> userManager, 
            SignInManager<User> signInManager, 
            JwtTokenHelper tokenHelper)
        {
            _logger = logger;
            _userManager = userManager;
            _signInManager = signInManager;
            _tokenHelper = tokenHelper;
        }

        public async Task<AuthResponse> RegisterAsync(CreateUserRequest createUser)
        {
            _ = createUser ?? throw new ArgumentNullException(nameof(createUser));
            
            if (string.IsNullOrEmpty(createUser.UserName))
                throw new FortCodeException(HttpStatusCode.BadRequest, $"{nameof(createUser.UserName)} is required.");
            
            if (string.IsNullOrEmpty(createUser.Email))
                throw new FortCodeException(HttpStatusCode.BadRequest, $"{nameof(createUser.Email)} is required.");
            
            if (string.IsNullOrEmpty(createUser.Password))
                throw new FortCodeException(HttpStatusCode.BadRequest, $"{nameof(createUser.Password)} is required.");
            
            var existingUser = await _userManager.FindByEmailAsync(createUser.Email);

            if (existingUser != null)
            {
                throw new FortCodeException(HttpStatusCode.Conflict, "A user with the specified email address already exists.");
            };

            var newUser = new User
            {
                Id = Guid.NewGuid(),
                UserName = createUser.UserName,
                Email = createUser.Email
            };
            var result = await _userManager.CreateAsync(newUser, createUser.Password);
            
            if (!result.Succeeded)
            {
                throw new FortCodeException(HttpStatusCode.InternalServerError, result.Errors?.FirstOrDefault().Description);
            }

            return GenerateAuthResponse(newUser);
        }

        public async Task<AuthResponse> AuthenticateAsync(AuthRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);

            if (user == null)
            {
                throw new FortCodeException(HttpStatusCode.NotFound, "A user with the specified credentials could not be found.");
            }
            
            var signInResult = await _signInManager.CheckPasswordSignInAsync(user, request.Password, false);
            
            if (!signInResult.Succeeded || signInResult.IsLockedOut)
            {
                throw new FortCodeException(HttpStatusCode.Unauthorized, "Email or password is incorrect.");
            }

            return GenerateAuthResponse(user);
        }

        private AuthResponse GenerateAuthResponse(User newUser)
        {
            return new AuthResponse
            {
                User = new UserResponse
                {
                    Id = newUser.Id,
                    Email = newUser.Email,
                    UserName = newUser.Email
                },
                Token = _tokenHelper.GenerateToken(newUser)
            };
        }

    }
}