﻿using System;
using System.Threading.Tasks;
using FortCode.Data.Interfaces;
using FortCode.Data.Models;
using FortCode.Services.Interfaces;
using Microsoft.Extensions.Logging;

namespace FortCode.Services
{
    public class UserService: IUserService
    {
        private readonly ILogger<UserService> _logger;
        private readonly IUserRepository _repository;

        public UserService(ILogger<UserService> logger, IUserRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        public async Task<User> GetUserAsync(Guid id)
        {
            return await _repository.GetByIdAsync(id);
        }
    }
}