﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FortCode.Models.Requests;
using FortCode.Models.Responses;

namespace FortCode.Services.Interfaces
{
    public interface IBarService
    {
        Task<IEnumerable<BarResponse>> GetAllBarsAsync();
        Task<IEnumerable<BarResponse>> GetFavoriteBarsAsync(Guid userId, Guid cityId);
        Task<IEnumerable<SharedFavoriteBarResponse>> GetSharedFavoriteBarsAsync(Guid userId, Guid cityId);
        Task AddFavoriteBarAsync(Guid userId, AddBarRequest request);
        Task DeleteFavoriteBarAsync(Guid userId, Guid barId);
    }
}