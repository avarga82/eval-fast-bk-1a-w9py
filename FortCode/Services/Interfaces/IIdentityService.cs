﻿using System.Threading.Tasks;
using FortCode.Models.Requests;
using FortCode.Models.Responses;

namespace FortCode.Services.Interfaces
{
    public interface IIdentityService
    {
        Task<AuthResponse> RegisterAsync(CreateUserRequest createUser);
        Task<AuthResponse> AuthenticateAsync(AuthRequest request);
    }
}