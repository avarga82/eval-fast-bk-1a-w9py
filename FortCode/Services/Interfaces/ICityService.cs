﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FortCode.Models.Responses;

namespace FortCode.Services.Interfaces
{
    public interface ICityService
    {
        Task<IEnumerable<CityResponse>> GetAllCitiesAsync();
        Task<IEnumerable<CityResponse>> GetFavoriteCitiesAsync(Guid userId);
        Task AddFavoriteCityAsync(Guid userId, Guid cityId);
        Task DeleteFavoriteCityAsync(Guid userId, Guid cityId);
    }
}