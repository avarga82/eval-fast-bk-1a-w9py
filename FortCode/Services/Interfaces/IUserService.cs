﻿using System;
using System.Threading.Tasks;
using FortCode.Data.Models;

namespace FortCode.Services.Interfaces
{
    public interface IUserService
    {
        Task<User> GetUserAsync(Guid id);
    }
}