﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FortCode.Common;
using FortCode.Data.Interfaces;
using FortCode.Data.Models;
using FortCode.Models.Requests;
using FortCode.Models.Responses;
using FortCode.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FortCode.Services
{
    public class BarService : IBarService
    {
        private readonly ILogger<BarService> _logger;
        private readonly IBarRepository _repository;
        private readonly IUserRepository _userRepository;
        private readonly ICityRepository _cityRepository;
        private readonly IUserBarRepository _userBarRepository;

        public BarService(
            ILogger<BarService> logger, 
            IBarRepository repository, 
            IUserRepository userRepository, 
            IUserBarRepository userBarRepository, 
            ICityRepository cityRepository)
        {
            _logger = logger;
            _repository = repository;
            _userRepository = userRepository;
            _userBarRepository = userBarRepository;
            _cityRepository = cityRepository;
        }

        public async Task<IEnumerable<BarResponse>> GetAllBarsAsync()
        {
            return await _repository.GetAll()
                .Include(c => c.City)
                .Include($"{nameof(City)}.{nameof(Country)}")
                .OrderBy(c => c.Name)
                .Select(c => new BarResponse
                {
                    Id = c.Id,
                    Name = c.Name,
                    City = new CityResponse
                    {
                        Id = c.CityId,
                        Name = c.City.Name,
                        Country = c.City.Country.Name
                    }
                }).ToListAsync();
        }

        public async Task<IEnumerable<BarResponse>> GetFavoriteBarsAsync(Guid userId, Guid cityId)
        {
            await ValidateUserAsync(userId);

            await ValidateCityAsync(cityId);

            var bars = await _repository.GetFavoriteBarsAsync(userId, cityId);

            return bars.Select(c => new BarResponse
            {
                Id = c.Id,
                Name = c.Name,
                City = new CityResponse
                {
                    Id = c.CityId,
                    Name = c.City.Name,
                    Country = c.City.Country.Name
                }
            });
        }
        
        public async Task<IEnumerable<SharedFavoriteBarResponse>> GetSharedFavoriteBarsAsync(Guid userId, Guid cityId)
        {
            await ValidateUserAsync(userId);

            await ValidateCityAsync(cityId);

            var favoriteBarsFromOtherUsers = await _userBarRepository.GetAll()
                .Include($"{nameof(Bar)}.{nameof(City)}")
                .Where(c => c.UserId != userId && c.Bar.CityId == cityId)
                .Select(c => c.BarId)
                .ToListAsync();
            
            var result = await _userBarRepository.GetAll()
                .Include($"{nameof(Bar)}.{nameof(City)}")
                .Where(c => c.UserId == userId && c.Bar.CityId == cityId && favoriteBarsFromOtherUsers.Contains(c.BarId))
                .Select(c => new SharedFavoriteBarResponse
                {
                    Id = c.BarId,
                    Name = c.Bar.Name
                })
                .ToListAsync();

            foreach (var item in result)
            {
                item.CountOfSharedFavorites = favoriteBarsFromOtherUsers.Count(b => b == item.Id);
            }

            return result;
        }

        public async Task AddFavoriteBarAsync(Guid userId, AddBarRequest request)
        {
            _ = request ?? throw new ArgumentNullException(nameof(request));
            
            await ValidateUserAsync(userId);

            var barExists = await _repository.ExistsAsync(u => u.Id == request.BarId);
            if (!barExists)
            {
                throw new FortCodeException(HttpStatusCode.NotFound, "A bar could not be found for the specified ID value.");
            }
            
            var itemExists = await _userBarRepository.ExistsAsync(u => u.UserId == userId && u.BarId == request.BarId);
            if (itemExists)
            {
                throw new FortCodeException(HttpStatusCode.Conflict, "This specified favorite bar already exists.");
            }

            await _userBarRepository.AddAsync(new UserBar
            {
                UserId = userId,
                BarId = request.BarId,
                FavoriteDrink = request.FavoriteDrink
            });
        }

        public async Task DeleteFavoriteBarAsync(Guid userId, Guid barId)
        {
            var userBar = await _userBarRepository.GetAll().FirstOrDefaultAsync(uc => uc.UserId == userId && uc.BarId == barId);
            if (userBar == null)
            {
                throw new FortCodeException(HttpStatusCode.NotFound, "The specified favorite bar could not be found.");
            }

            await _userBarRepository.DeleteAsync(userBar.Id);
        }

        private async Task ValidateCityAsync(Guid cityId)
        {
            var cityExists = await _cityRepository.ExistsAsync(u => u.Id == cityId);
            if (!cityExists)
            {
                throw new FortCodeException(HttpStatusCode.NotFound, "A city could not be found for the specified ID value.");
            }
        }

        private async Task ValidateUserAsync(Guid userId)
        {
            var userExists = await _userRepository.ExistsAsync(u => u.Id == userId);
            if (!userExists)
            {
                throw new FortCodeException(HttpStatusCode.NotFound, "A user could not be found for the specified ID value.");
            }
        }
    }
}