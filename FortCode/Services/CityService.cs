﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FortCode.Common;
using FortCode.Data.Interfaces;
using FortCode.Data.Models;
using FortCode.Models.Responses;
using FortCode.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FortCode.Services
{
    public class CityService : ICityService
    {
        private readonly ILogger<CityService> _logger;
        private readonly ICityRepository _repository;
        private readonly IUserRepository _userRepository;
        private readonly IUserCityRepository _userCityRepository;

        public CityService(
            ILogger<CityService> logger, 
            ICityRepository repository, 
            IUserRepository userRepository, 
            IUserCityRepository userCityRepository)
        {
            _logger = logger;
            _repository = repository;
            _userRepository = userRepository;
            _userCityRepository = userCityRepository;
        }

        public async Task<IEnumerable<CityResponse>> GetAllCitiesAsync()
        {
            return await _repository.GetAll()
                .OrderBy(c => c.Name)
                .Select(c => new CityResponse
                {
                    Id = c.Id,
                    Name = c.Name,
                    Country = c.Country.Name
                }).ToListAsync();
        }

        public async Task<IEnumerable<CityResponse>> GetFavoriteCitiesAsync(Guid userId)
        {
            var userExists = await _userRepository.ExistsAsync(u => u.Id == userId);
            if (!userExists)
            {
                throw new FortCodeException(HttpStatusCode.NotFound, "A user could not be found for the specified ID value.");
            }

            var cities = await _repository.GetFavoriteCities(userId);

            return cities.Select(c => new CityResponse
            {
                Id = c.Id,
                Name = c.Name,
                Country = c.Country.Name
            });
        }

        public async Task AddFavoriteCityAsync(Guid userId, Guid cityId)
        {
            var userExists = await _userRepository.ExistsAsync(u => u.Id == userId);
            if (!userExists)
            {
                throw new FortCodeException(HttpStatusCode.NotFound, "A user could not be found for the specified ID value.");
            }

            var cityExists = await _repository.ExistsAsync(u => u.Id == cityId);
            if (!cityExists)
            {
                throw new FortCodeException(HttpStatusCode.NotFound, "A city could not be found for the specified ID value.");
            }
            
            var itemExists = await _userCityRepository.ExistsAsync(u => u.UserId == userId && u.CityId == cityId);
            if (itemExists)
            {
                throw new FortCodeException(HttpStatusCode.Conflict, "This specified favorite city already exists.");
            }

            await _userCityRepository.AddAsync(new UserCity
            {
                UserId = userId,
                CityId = cityId
            });
        }

        public async Task DeleteFavoriteCityAsync(Guid userId, Guid cityId)
        {
            var userCity = await _userCityRepository.GetAll().FirstOrDefaultAsync(uc => uc.UserId == userId && uc.CityId == cityId);
            if (userCity == null)
            {
                throw new FortCodeException(HttpStatusCode.NotFound, "The specified user/city could not be found.");
            }

            await _userCityRepository.DeleteAsync(userCity.Id);
        }
    }
}