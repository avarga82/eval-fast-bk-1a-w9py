using System;
using System.Net;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using FortCode.Api.Controllers;
using FortCode.Configuration;
using FortCode.Data.Models;
using FortCode.Models.Responses;
using FortCode.Services.Interfaces;
using FortCode.Utils;
using GST.Fake.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FortCode.Tests
{

    [TestClass]
    public class CityControllerTests
    {
        private CityController _sut;
        private Mock<ICityService> _cityService;
        private JwtTokenHelper _tokenHelper;
        private readonly Fixture _fixture = new Fixture();
        private CustomWebApplicationFactory _factory;

        [TestInitialize]
        public void Setup()
        {
            var logger = new Mock<ILogger<CityController>>();
            var options = Options.Create(_fixture.Create<JwtSettings>());

            _cityService = new Mock<ICityService>();
            _sut = new CityController(logger.Object, _cityService.Object);
            _tokenHelper = new JwtTokenHelper(options);
            _factory = new CustomWebApplicationFactory();
        }

        [TestMethod]
        public async Task Get_ShouldReturnOkResultWithData()
        {
            // Arrange
            var cities = _fixture.CreateMany<CityResponse>();
            _cityService
                .Setup(x => x.GetAllCitiesAsync())
                .Returns(Task.FromResult(cities));

            // Act
            var actionResult = await _sut.Get();

            // Assert
            actionResult.Should().NotBeNull();
            actionResult.Result.Should().NotBeNull();
            actionResult.Result.Should().BeOfType<OkObjectResult>();
            (actionResult.Result as OkObjectResult).StatusCode.Should().Be((int)HttpStatusCode.OK);
            (actionResult.Result as OkObjectResult).Value.Should().NotBeNull();
            (actionResult.Result as OkObjectResult).Value.Should().BeEquivalentTo(cities);
        }

        [TestMethod]
        public async Task Get_ShouldReturnOkResult_WhenAuthTokenSet()
        {
            // Arrange
            var client = _factory.CreateClient();
            var user = _fixture.Create<User>();
            var token = _tokenHelper.GenerateToken(user);

            client.SetFakeBearerToken(token);

            // Act
            var response = await client.GetAsync("api/city");

            // Assert
            response.Should().NotBeNull();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task Get_ShouldReturnUnauthorized_WhenAuthTokenNotSet()
        {
            // Arrange
            var client = _factory.CreateClient();

            // Act
            var response = await client.GetAsync("api/city");

            // Assert
            response.Should().NotBeNull();
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public async Task GetFavorites_ShouldReturnOkResultWithData_WhenCityFound()
        {
            // Arrange
            var cities = _fixture.CreateMany<CityResponse>();
            _cityService
                .Setup(x => x.GetFavoriteCitiesAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(cities));

            // Act
            var actionResult = await _sut.GetFavorites();

            // Assert
            actionResult.Should().NotBeNull();
            actionResult.Result.Should().NotBeNull();
            actionResult.Result.Should().BeOfType<OkObjectResult>();
            (actionResult.Result as OkObjectResult).StatusCode.Should().Be((int)HttpStatusCode.OK);
            (actionResult.Result as OkObjectResult).Value.Should().NotBeNull();
            (actionResult.Result as OkObjectResult).Value.Should().BeEquivalentTo(cities);
        }

        [TestMethod]
        public async Task Post_ShouldReturnNoContentResult()
        {
            // Arrange

            // Act
            var actionResult = await _sut.AddFavorite(It.IsAny<Guid>());

            // Assert
            actionResult.Should().NotBeNull();
            actionResult.Should().BeOfType<NoContentResult>();
            (actionResult as NoContentResult).StatusCode.Should().Be((int)HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task Delete_ShouldReturnNoContentResult()
        {
            // Arrange

            // Act
            var actionResult = await _sut.DeleteFavorite(It.IsAny<Guid>());

            // Assert
            actionResult.Should().NotBeNull();
            actionResult.Should().BeOfType<NoContentResult>();
            (actionResult as NoContentResult).StatusCode.Should().Be((int)HttpStatusCode.NoContent);
        }
    }
}